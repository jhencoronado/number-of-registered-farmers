VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} UserForm1 
   Caption         =   "Number of Registered Farmers by Sex, by Main Purpose for Raising Livestock 2013"
   ClientHeight    =   3975
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   7350
   OleObjectBlob   =   "UserForm1.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "UserForm1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdAveF_Click()
    Dim ave As Double, val As Double
    val = Range("E4").Value
    ave = val / 20
    MsgBox ave, vbInformation + vbOKOnly, "Average No. of Females"
    
End Sub

Private Sub cmdBestP_Click()
    Dim prov1, prov2, prov3, prov4, prov5, prov6, prov7, prov8, prov9 As String
    Dim maxVal1, maxVal2, maxVal3, maxVal4, maxVal5, maxVal6, maxVal7, maxVal8, maxVal9 As Double
    Dim rng1, rng2, rng3, rng4, rng5, rng6, rng7, rng8, rng9 As Range
    Dim rowVal1, rowVal2, rowVal3, rowVal4, rowVal5, rowVal6, rowVal7, rowVal8, rowVal9 As Integer
   'col1
    Set rng1 = Sheet1.Range("C8:C87")
    maxVal1 = Application.WorksheetFunction.Max(rng1)
    Cells.Find(maxVal1).Activate
    rowVal1 = ActiveCell.Row
    prov1 = Range("A" & rowVal1).Value
    'col2
    Set rng2 = Sheet1.Range("D8:D87")
    maxVal2 = Application.WorksheetFunction.Max(rng2)
    Cells.Find(maxVal2).Activate
    rowVal2 = ActiveCell.Row
    prov2 = Range("A" & rowVal2).Value
    'col3
    Set rng3 = Sheet1.Range("E8:E87")
    maxVal3 = Application.WorksheetFunction.Max(rng3)
    Cells.Find(maxVal3).Activate
    rowVal3 = ActiveCell.Row
    prov3 = Range("A" & rowVal3).Value
    'col4
    Set rng4 = Sheet1.Range("F8:F87")
    maxVal4 = Application.WorksheetFunction.Max(rng4)
    Cells.Find(maxVal4).Activate
    rowVal4 = ActiveCell.Row
    prov4 = Range("A" & rowVal4).Value
    'col5
    Set rng5 = Sheet1.Range("G8:G87")
    maxVal5 = Application.WorksheetFunction.Max(rng5)
    Cells.Find(maxVal5).Activate
    rowVal5 = ActiveCell.Row
    prov5 = Range("A" & rowVal5).Value
    'col6
    Set rng6 = Sheet1.Range("H8:H87")
    maxVal6 = Application.WorksheetFunction.Max(rng6)
    Cells.Find(maxVal6).Activate
    rowVal6 = ActiveCell.Row
    prov6 = Range("A" & rowVal6).Value
    'col7
    Set rng7 = Sheet1.Range("I8:I87")
    maxVal7 = Application.WorksheetFunction.Max(rng7)
    Cells.Find(maxVal7).Activate
    rowVal7 = ActiveCell.Row
    prov7 = Range("A" & rowVal7).Value
    'col8
    Set rng8 = Sheet1.Range("J8:J87")
    maxVal8 = Application.WorksheetFunction.Max(rng8)
    Cells.Find(maxVal8).Activate
    rowVal8 = ActiveCell.Row
    prov8 = Range("A" & rowVal8).Value
    'col9
    Set rng9 = Sheet1.Range("K8:K87")
    maxVal9 = Application.WorksheetFunction.Max(rng9)
    Cells.Find(maxVal9).Activate
    rowVal9 = ActiveCell.Row
    prov9 = Range("A" & rowVal9).Value
    
   MsgBox "Best Province in Total Registered Farmers: " & prov1 & vbNewLine & "Best Province in Total Registered Male Farmers: " & prov2 & vbNewLine & "Best Province in Total Registered Female Farmers: " & prov3 & vbNewLine & "Best Province in Total No. of Registered Farmers only: " & prov4 & vbNewLine & "Best Province in No. of Registered Male Farmers only: " & prov5 & vbNewLine & "Best Province in No. of Registered Female Farmers only: " & prov6 & vbNewLine & "Best Province in Total No. of Registered Farmers also Farm Laborers and/or Fishermen: " & prov7 & vbNewLine & "Best Province in No. of Registered Male Farmers also Farm Laborers and/or Fishermen: " & prov8 & vbNewLine & "Best Province in No. of Registered Female Farmers also Farm Laborers and/or Fishermen: " & prov9, vbInformation + vbOKOnly, "Best Province"
     
End Sub

Private Sub cmdProvF_Click()
    Dim rng As Range
    Dim maxVal As Double
    Dim rowVal As Integer
    Dim prov As String
    Set rng = Sheet1.Range("E8:E87")
    maxVal = Application.WorksheetFunction.Max(rng)
    Cells.Find(maxVal).Activate
    rowVal = ActiveCell.Row
    prov = Range("A" & rowVal).Value
    MsgBox prov, vbInformation + vbOKOnly, "Province With Many Females"
End Sub

